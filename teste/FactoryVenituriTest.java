package teste;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import classes.*;

public class FactoryVenituriTest {


	public FactoryVenituri factoryVenituri = null;
	public int idJudet= 0;
	public double venituriTotaleInitiale = 0.0;

	
	@Before
	public void setUp() throws Exception {
		idJudet = 1;
		factoryVenituri = new FactoryVenituri(idJudet);
		
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testAdunaZero() throws ExceptieNumarMare, ExceptieNumarMic {
		
		double venituriAsteptate = factoryVenituri.getVenituriTotale() + 0;
		factoryVenituri.aduna(0);
		
		
		assertEquals("Test aduna 0 la venituri totale",venituriAsteptate,factoryVenituri.getVenituriTotale(),0);
	}
	
	@Test
	public void testAdunaNumarNormal() throws ExceptieNumarMare, ExceptieNumarMic {
		double numarNormal =5;
		double venituriAsteptate = factoryVenituri.getVenituriTotale() + numarNormal;
		factoryVenituri.aduna(numarNormal);
		
		
		assertEquals("Test aduna numar normal la venituri totale",venituriAsteptate,factoryVenituri.getVenituriTotale(),0);
	}

	//MAX_VALUE
	@Test
	public void testAdunaNumarMare() throws ExceptieNumarMare {
		double numarMare = Double.MAX_VALUE;
		try{
			factoryVenituri.aduna(numarMare);
			fail();
		}
		catch(ExceptieNumarMare ex){
			
		}
		catch(Exception e){
			
		}		
		
	}
	
	//MIN_VALUE
	@Test
	public void testAdunaNumarMic() throws ExceptieNumarMic {
		double numarMic = Double.MIN_VALUE;
		try{
			factoryVenituri.aduna(numarMic);
			fail();
		}
		catch(ExceptieNumarMic ex){
			
		}
		catch(Exception e){
			
		}
	}
	
	//inverse check
		@Test
		public void testAdunaInverseCheck() throws ExceptieNumarMare, ExceptieNumarMic {				
				
			double numarNormal =5;
			double venituriInitiale = factoryVenituri.getVenituriTotale();
			factoryVenituri.aduna(numarNormal);			
			assertEquals("Test aduna numar normal la venituri totale - inverse check",venituriInitiale,factoryVenituri.getVenituriTotale()-numarNormal,0);
		}
		
		//crossCheck
		@Test
		public void testSumaCrossCheck() throws ExceptieNumarMare, ExceptieNumarMic {
				
			double numarNormal =5;
			double venituriInitiale = factoryVenituri.getVenituriTotale();
			factoryVenituri.aduna(numarNormal);			
			assertEquals("Test aduna numar normal la venituri totale - cross check",venituriInitiale + numarNormal,factoryVenituri.getVenituriTotale(),0);
		}
	
	@Test
	public void testScadeZero() throws ExceptieNumarMare, ExceptieNumarMic {
		
		double venituriAsteptate = factoryVenituri.getVenituriTotale() + 0;
		factoryVenituri.scade(0);
		
		
		assertEquals("Test aduna 0 la venituri totale",venituriAsteptate,factoryVenituri.getCheltuieliTotale(),0);
	}
	
	@Test
	public void testScadeNumarNormal() throws ExceptieNumarMare, ExceptieNumarMic {
		double numarNormal =5;
		double venituriAsteptate = factoryVenituri.getVenituriTotale() - numarNormal;
		factoryVenituri.scade(numarNormal);
		
		
		assertEquals("Test aduna numar normal la venituri totale",venituriAsteptate,factoryVenituri.getVenituriTotale(),0);
	}

	//MAX_VALUE
	@Test
	public void testScadeNumarMare() throws ExceptieNumarMare {
		double numarMare = Double.MAX_VALUE;
		try{
			factoryVenituri.scade(numarMare);
			fail();
		}
		catch(ExceptieNumarMare ex){
			
		}
		catch(Exception e){
			
		}		
		
	}
	
	//MIN_VALUE
	@Test
	public void testScadeNumarMic() throws ExceptieNumarMic {
		double numarMic = Double.MIN_VALUE;
		try{
			factoryVenituri.aduna(numarMic);
			fail();
		}
		catch(ExceptieNumarMic ex){
			
		}
		catch(Exception e){
			
		}
	}
	@Test
	public void testCreateObject() {
		
		String tip = "Venit curenta";
		Venit venitNou = new Venit(tip);
		Venit venitCreat = (Venit) factoryVenituri.createObject(tip);
		assertEquals(venitNou.getTipVenit(),venitCreat.getTipVenit());
	}


}
