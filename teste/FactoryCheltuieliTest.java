package teste;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import classes.*;

public class FactoryCheltuieliTest {

	
	public FactoryCheltuieli factoryCheltuieli = null;
	public int idJudet= 0;
	public double cheltuieliTotaleInitiale = 0.0;

	
	@Before
	public void setUp() throws Exception {
		idJudet = 1;
		factoryCheltuieli = new FactoryCheltuieli(idJudet);
		
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testAdunaZero() throws ExceptieNumarMare, ExceptieNumarMic {
		
		double cheltuieliAsteptate = factoryCheltuieli.getCheltuieliTotale() + 0;
		factoryCheltuieli.aduna(0);
		
		
		assertEquals("Test aduna 0 la cheltuieli totale",cheltuieliAsteptate,factoryCheltuieli.getCheltuieliTotale(),0);
	}
	
	@Test
	public void testAdunaNumarNormal() throws ExceptieNumarMare, ExceptieNumarMic {
		double numarNormal =5;
		double cheltuieliAsteptate = factoryCheltuieli.getCheltuieliTotale() + numarNormal;
		factoryCheltuieli.aduna(numarNormal);
		
		
		assertEquals("Test aduna numar normal la cheltuieli totale",cheltuieliAsteptate,factoryCheltuieli.getCheltuieliTotale(),0);
	}

	//MAX_VALUE
	@Test
	public void testAdunaNumarMare() throws ExceptieNumarMare {
		double numarMare = Double.MAX_VALUE;
		try{
			factoryCheltuieli.aduna(numarMare);
			fail();
		}
		catch(ExceptieNumarMare ex){
			
		}
		catch(Exception e){
			
		}		
		
	}
	
	//MIN_VALUE
	@Test
	public void testAdunaNumarMic() throws ExceptieNumarMic {
		double numarMic = Double.MIN_VALUE;
		try{
			factoryCheltuieli.aduna(numarMic);
			fail();
		}
		catch(ExceptieNumarMic ex){
			
		}
		catch(Exception e){
			
		}
	}
	
	@Test
	public void testAdunaNumarNegativ() throws ExceptieNumarMare, ExceptieNumarMic{
		double numarNegativ = -4.5;
		double cheltuieliTotaleAsteptate = factoryCheltuieli.getCheltuieliTotale() + numarNegativ;
		factoryCheltuieli.aduna(numarNegativ);
		
		assertEquals("Test aduna numar negativ - cheltuieliFactory", cheltuieliTotaleAsteptate,factoryCheltuieli.getCheltuieliTotale(),0);
	}
	
	//inverse check
	@Test
	public void testAdunaInverseCheck() throws ExceptieNumarMare, ExceptieNumarMic {				
			
		double numarNormal =5;
		double cheltuieliInitiale = factoryCheltuieli.getCheltuieliTotale();
		factoryCheltuieli.aduna(numarNormal);			
		assertEquals("Test aduna numar normal la cheltuieli totale - inverse check",cheltuieliInitiale,factoryCheltuieli.getCheltuieliTotale()-numarNormal,0);
	}
	
	//crossCheck
	@Test
	public void testSumaCrossCheck() throws ExceptieNumarMare, ExceptieNumarMic {
			
		double numarNormal =5;
		double cheltuieliInitiale = factoryCheltuieli.getCheltuieliTotale();
		factoryCheltuieli.aduna(numarNormal);			
		assertEquals("Test aduna numar normal la cheltuieli totale - cross check",cheltuieliInitiale + numarNormal,factoryCheltuieli.getCheltuieliTotale(),0);
	}
	
	@Test
	public void testScadeZero() throws ExceptieNumarMare, ExceptieNumarMic {
		
		double cheltuieliAsteptate = factoryCheltuieli.getCheltuieliTotale() + 0;
		factoryCheltuieli.scade(0);
		
		
		assertEquals("Test aduna 0 la cheltuieli totale",cheltuieliAsteptate,factoryCheltuieli.getCheltuieliTotale(),0);
	}
	
	@Test
	public void testScadeNumarNormal() throws ExceptieNumarMare, ExceptieNumarMic {
		double numarNormal =5;
		double cheltuieliAsteptate = factoryCheltuieli.getCheltuieliTotale() - numarNormal;
		factoryCheltuieli.scade(numarNormal);
		
		
		assertEquals("Test aduna numar normal la cheltuieli totale",cheltuieliAsteptate,factoryCheltuieli.getCheltuieliTotale(),0);
	}

	//MAX_VALUE
	@Test
	public void testScadeNumarMare() throws ExceptieNumarMare {
		double numarMare = Double.MAX_VALUE;
		try{
			factoryCheltuieli.scade(numarMare);
			fail();
		}
		catch(ExceptieNumarMare ex){
			
		}
		catch(Exception e){
			
		}		
		
	}
	
	//MIN_VALUE
	@Test
	public void testScadeNumarMic() throws ExceptieNumarMic {
		double numarMic = Double.MIN_VALUE;
		try{
			factoryCheltuieli.aduna(numarMic);
			fail();
		}
		catch(ExceptieNumarMic ex){
			
		}
		catch(Exception e){
			
		}
	}
	@Test
	public void testCreateObject() {
		
		String tip = "Cheltuiala curenta";
		Cheltuiala cheltuialaNoua = new Cheltuiala(tip);
		Cheltuiala cheltuialaCreata = (Cheltuiala) factoryCheltuieli.createObject(tip);
		assertEquals(cheltuialaNoua.getTipCheltuiala(),cheltuialaCreata.getTipCheltuiala());
	}

	
	

}
